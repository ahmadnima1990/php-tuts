<?php
// Declareing NameSpace
namespace Ahmad;

const MYPRO = 12;
class Nima {}
function kian(){}
?>

<?php 
// Declareing Sub NameSpace

namespace Ahmad\Sub\level;

const MYPRO = 12; // Ahmad\Sub\level\MYPRO
class Nima {} // Ahmad\Sub\level\Nima
function kian(){} // Ahmad\Sub\level\connect

?>

<?php 

namespace MyFirstProject;

const CONNECT_OK = 1;
class Connection { /* ... */ }
function connect() { /* ... */  }

namespace MySecondProject;

const CONNECT_OK = 1;
class Connection { /* ... */ }
function connect() { /* ... */  }


?>
<?php 
// Global Code
namespace {
	// .........
}

?>