
<?php
// User-defined functions

// Conditional functions
$c = true;

a();

if ($c) {
  function b()
  {
    echo " function b ";
  }
}

if ($c) b();

function a() 
{
  echo " function a ";
}

?>


<br />
<br />


<?php
function ahmad() 
{
  function nima() 
  {
	  echo 'Nima func.';
	  
	  function kian(){
		  echo "Kian func.";
	  }
    
  }
}

ahmad();
nima();
kian();

?>


<br />
<br />


<?php
// Function Args

function collect($num1, $num2, $str1, $str2) {
	echo $num1 * $num2;
	echo $str1 . $str2;
}

collect(10, 22, 'hello', 'world');

echo '<br />';

function cars($type = "maxima")
{
    return "I love cars like $type.\n";
}
echo cars();
echo cars(null);
echo cars("BMW");
?>


<br />
<br />

<?php
// Using ... to access args
function names(...$names) {
    $n = null;
    foreach ($names as $name) {
        $n .= $name;
    }
    return $n;
}

echo names('Ahmad', 'Nima', 'Kian', 'Behnam');
// AhmadNimaKianBehnam
?>


<br />
<br />


<?php
// Variable function
function sos(){
	echo 'Hi MOM';
}
$name = 'sos';
$name();

echo '<br />';
echo '<br />';

class Ahmad {
	function Nima(){
		return 'HI To My Friends';
	}
}
$abc = new Ahmad();
$funcName = 'Nima';
echo $abc->$funcName();

?>

<br />
<br />

<?php 
// Anonymous function

$sayHello = function($name)
{
    echo 'Hello '.$name.'<br />';
	
};

$sayHello('World');
$sayHello('PHP');


?>

<?php 
// Arrow Functions

$items = fn($x, $y) => $x * $y;
echo $items(12, 4);

?>