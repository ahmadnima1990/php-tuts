

<?php
class myFirstClass
{
    // property declaration
    public string $name = 'ahmad gholamnia';

    // method declaration
    public function displayName() {
        echo $this->name;
    }
}

$fname = new myFirstClass();
$fname->displayName();



$myDate = (new DateTime());
echo $myDate->format('M'); // Apr

?>

<br/>

<?php

// Object Inheritance 
class Ahmad
{
    public function printItem($string)
    {
        echo 'Ahmad: ' . $string ;
    }
    
    public function printPHP()
    {
        echo 'PHP is great.' ;
    }
}

class Nima extends Ahmad
{
    public function printItem($string)
    {
        echo 'Nima: ' . $string ;
    }
}

$ahmad = new Ahmad();
$nima = new Nima();
$ahmad->printItem('Gholamnia'); 
$ahmad->printPHP();       
$nima->printItem('Gholamnia'); 
$nima->printPHP();       


echo '<br />';
echo '<br />';


// Scope Resolution ::
class MyClass {
    const constVal = 'this is a constant value';
}

$classname = 'MyClass';
echo $classname::constVal;

echo MyClass::constVal;

?>